package com.xj.DataProtocol;

import static com.xj.DataProtocol.ByteUtils.joinByteArray;

import android.os.Build;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class XJDataProtocol {
    //type id
    public static final byte PROTOCOL_PROP_TYPE_BOOL = 0;
    public static final byte PROTOCOL_PROP_TYPE_INT8 = 1;
    public static final byte PROTOCOL_PROP_TYPE_UINT8 = 2;
    public static final byte PROTOCOL_PROP_TYPE_INT16 = 3;
    public static final byte PROTOCOL_PROP_TYPE_UINT16 = 4;
    public static final byte PROTOCOL_PROP_TYPE_INT32 = 5;
    public static final byte PROTOCOL_PROP_TYPE_UINT32 = 6;
    public static final byte PROTOCOL_PROP_TYPE_STRING = 11;
    public static final byte PROTOCOL_PROP_TYPE_ARRAY = 14;
    //cmd

    public static final byte CMD_DOWN_VERIFY = (byte) 0x01;
    public static final byte CMD_DOWN_INIT = (byte) 0x02;
    public static final byte CMD_DOWN_REPORT = (byte) 0x03;
    public static final byte CMD_DOWN_SET = (byte) 0x81;
    public static final byte CMD_DOWN_GET = (byte) 0x82;
    public static final byte CMD_DOWN_SNAPSHOT = (byte) 0x84;
    public static final byte CMD_DOWN_WIFICONFIG = (byte) 0x85;
    public static final byte CMD_DOWN_RANDOM = (byte) 0x86;
    public static final byte CMD_DOWN_BIND = (byte) 0x87;

    //ota
    public static final byte CMD_DOWN_VERSION = (byte) 0xA0;
    public static final byte CMD_DOWN_UPDATE_REQUEST = (byte) 0xA1;
    public static final byte CMD_DOWN_SEND_DATA = (byte) 0xA2;
    public static final byte CMD_DOWN_CHECK = (byte) 0xA3;


    //attrIds
    public static final byte ATTR_ID_SSID = (byte) 0xF5;
    public static final byte ATTR_ID_PASSWORD = (byte) 0xF4;
    public static final byte ATTR_ID_BSSID = (byte) 0xF3;
    public static final byte ATTR_ID_TOKEN = (byte) 0xF2;
    public static final byte ATTR_ID_AREA = (byte) 0xEF;
    public static final byte ATTR_URL_AREA = (byte) 0xEE;
    public static final byte ATTR_TYPE_AREA = (byte) 0xED;

    public enum XJPropDecodeOption {
        none,
        decodeByteArrayToHexString
    }

    public static byte[] getBoolPayload(byte attrId, boolean value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(3);
        byteBuffer.put(PROTOCOL_PROP_TYPE_BOOL);
        byteBuffer.put(attrId);
        byteBuffer.put((byte) (value ? 1 : 0));
        return byteBuffer.array();
    }

    public static byte[] getInt8Payload(byte attrId, byte value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(3);
        byteBuffer.put(PROTOCOL_PROP_TYPE_INT8);
        byteBuffer.put(attrId);
        byteBuffer.put(value);
        return byteBuffer.array();
    }

    public static byte[] getUint8Payload(byte attrId, byte value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(3);
        byteBuffer.put(PROTOCOL_PROP_TYPE_UINT8);
        byteBuffer.put(attrId);
        byteBuffer.put(value);
        return byteBuffer.array();
    }

    public static byte[] getInt16Payload(byte attrId, short value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.put(PROTOCOL_PROP_TYPE_INT16);
        byteBuffer.put(attrId);
        byteBuffer.put(ByteUtils.shortToBytes(value));
        return byteBuffer.array();
    }

    public static byte[] getUint16Payload(byte attrId, short value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.put(PROTOCOL_PROP_TYPE_UINT16);
        byteBuffer.put(attrId);
        byteBuffer.put(ByteUtils.shortToBytes(value));
        return byteBuffer.array();
    }

    public static byte[] getInt32Payload(byte attrId, int value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(6);
        byteBuffer.put(PROTOCOL_PROP_TYPE_INT32);
        byteBuffer.put(attrId);
        byteBuffer.put(ByteUtils.intToBytes(value));
        return byteBuffer.array();
    }

    public static byte[] getUint32Payload(byte attrId, int value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(6);
        byteBuffer.put(PROTOCOL_PROP_TYPE_UINT32);
        byteBuffer.put(attrId);
        byteBuffer.put(ByteUtils.intToBytes(value));
        return byteBuffer.array();
    }

    public static byte[] getStringPayload(byte attrId, String value) {
        byte[] bytes = value.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.allocate(4 + bytes.length);
        byteBuffer.put(PROTOCOL_PROP_TYPE_STRING);
        byteBuffer.put(attrId);
        byteBuffer.putShort((short) value.length());
        byteBuffer.put(bytes);
        return byteBuffer.array();
    }

    public static byte[] getArrayPayload(byte attrId, byte[] value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4 + value.length);
        byteBuffer.put(PROTOCOL_PROP_TYPE_ARRAY);
        byteBuffer.put(attrId);
        byteBuffer.putShort((short) value.length);
        byteBuffer.put(value);
        return byteBuffer.array();
    }

    public static byte[] encode(List<XJProp> props) {
        byte[] allBytes = new byte[0];
        if (props.size() > 0) {
            for (int i = 0; i < props.size(); i++) {
                XJProp xjProp = props.get(i);
                int value = Double.valueOf(String.valueOf(xjProp.getValue())).intValue();
                byte[] oneByte = setData(xjProp.getTypeId(), xjProp.getAttrId(), (byte) value);
                if (oneByte != null) {
                    allBytes = joinByteArray(allBytes, oneByte);
                }
            }
        }
        return allBytes;
    }

    public static Map<Object, Object> decodeToMap(byte[] payload) {
        return decodeToMap(payload, XJPropDecodeOption.none);
    }

    public static Map<Object, Object> decodeToMap(byte[] payload, XJPropDecodeOption option) {
        List<XJProp> list = decode(payload, option);
        Map<Object, Object> map = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            String attrId = list.get(i).getAttrId() + "";
            Object value = list.get(i).getValue();
            map.put(attrId, value);
        }
        return map;
    }

    public static List<XJProp> decode(byte[] payload) {
        return decode(payload, XJPropDecodeOption.none);
    }

    public static List<XJProp> decode(byte[] payload, XJPropDecodeOption option) {
        List<XJProp> xjProps = new ArrayList<>();
        ByteBuffer buffer = ByteBuffer.wrap(payload);
        while (buffer.hasRemaining()) {
            XJProp xjProp = new XJProp();
            byte typeId = buffer.get();
            byte attrId = buffer.get();
            xjProp.setTypeId(typeId);
            xjProp.setAttrId(attrId);
            switch (typeId) {
                case PROTOCOL_PROP_TYPE_BOOL:
                case PROTOCOL_PROP_TYPE_UINT8:
                    int boolValue = ByteUtils.getUnsignedByte(buffer.get());
                    xjProp.setValue(boolValue);
                    xjProps.add(xjProp);
                    break;
                case PROTOCOL_PROP_TYPE_INT8:
                    int int8Value = buffer.get();
                    xjProp.setValue(int8Value);
                    xjProps.add(xjProp);
                    break;
                case PROTOCOL_PROP_TYPE_INT16:
                    int int16Value = buffer.getShort();
                    xjProp.setValue(int16Value);
                    xjProps.add(xjProp);
                    break;
                case PROTOCOL_PROP_TYPE_UINT16:
                    int uint16Value = ByteUtils.getUnsignedShort(buffer.getShort());
                    xjProp.setValue(uint16Value);
                    xjProps.add(xjProp);
                    break;
                case PROTOCOL_PROP_TYPE_INT32:
                    int int32Value = buffer.getInt();
                    xjProp.setValue(int32Value);
                    xjProps.add(xjProp);
                    break;
                case PROTOCOL_PROP_TYPE_UINT32:
                    long uint32Value = ByteUtils.getUnsignedInt(buffer.getInt());
                    xjProp.setValue(uint32Value);
                    xjProps.add(xjProp);
                    break;
                case PROTOCOL_PROP_TYPE_STRING:
                    int StrLength = ByteUtils.getUnsignedShort(buffer.getShort());
                    byte[] strings = new byte[StrLength];
                    buffer.get(strings, 0, StrLength);
                    String StringValue = new String(strings);
                    xjProp.setValue(StringValue);
                    xjProps.add(xjProp);
                    break;
                case PROTOCOL_PROP_TYPE_ARRAY:
                    int arrayLength = ByteUtils.getUnsignedShort(buffer.getShort());
                    byte[] Arrays = new byte[arrayLength];
                    buffer.get(Arrays, 0, arrayLength);
                    if (option == XJPropDecodeOption.decodeByteArrayToHexString) {
                        String arrayBuffer = ByteUtils.bytesToHexString(Arrays).replace(" ", "");
                        xjProp.setValue(arrayBuffer);
                    } else {
                        xjProp.setValue(Arrays);
                    }
                    xjProps.add(xjProp);
                    break;
                default:
            }
        }
        return xjProps;
    }


    public static byte[] setData(byte typeId, byte attrId, byte value) {
        switch (typeId) {
            case PROTOCOL_PROP_TYPE_BOOL:
            case PROTOCOL_PROP_TYPE_UINT8:
                return getUint8Payload(attrId, value);
            case PROTOCOL_PROP_TYPE_INT8:
                return getInt8Payload(attrId, value);
            case PROTOCOL_PROP_TYPE_INT16:
                return getInt16Payload(attrId, value);
            case PROTOCOL_PROP_TYPE_UINT16:
                return getUint16Payload(attrId, value);
            case PROTOCOL_PROP_TYPE_INT32:
                return getInt32Payload(attrId, value);
            case PROTOCOL_PROP_TYPE_UINT32:
                return getUint32Payload(attrId, value);
            case PROTOCOL_PROP_TYPE_STRING:
                return getStringPayload(attrId, ByteUtils.byteToHexString(value));
            case PROTOCOL_PROP_TYPE_ARRAY:
                return getArrayPayload(attrId, ByteUtils.byteToBytes(value));
            default:
                return null;
        }
    }
}
