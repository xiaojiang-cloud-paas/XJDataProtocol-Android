package com.xj.DataProtocol;

/**
 * Created by facexxyz on 5/13/21.
 */


public class XJProp {
    private byte typeId;
    private byte attrId;
    private Object value;

    public byte getTypeId() {
        return typeId;
    }

    public XJProp() {
    }
    public XJProp(byte typeId, byte attrId, Object value) {
        this.typeId = typeId;
        this.attrId = attrId;
        this.value = value;
    }

    public void setTypeId(byte typeId) {
        this.typeId = typeId;
    }

    public byte getAttrId() {
        return attrId;
    }

    public void setAttrId(byte attrId) {
        this.attrId = attrId;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AttrModel{" +
                "typeId=" + typeId +
                ", attrId=" + attrId +
                ", value='" + value + '\'' +
                '}';
    }
}
